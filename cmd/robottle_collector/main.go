package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/micro/cli"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/client"

	accountProto "gitlab.com/robottle/robottle_account/proto/account"
	commonConfig "gitlab.com/robottle/robottle_common/config"
	commonConnection "gitlab.com/robottle/robottle_common/connection"
	commonConsumer "gitlab.com/robottle/robottle_common/consumer"

	"gitlab.com/robottle/robottle_collector/handler"
	"gitlab.com/robottle/robottle_collector/option"
	proto "gitlab.com/robottle/robottle_collector/proto/collector"
	"gitlab.com/robottle/robottle_collector/repository"
	"gitlab.com/robottle/robottle_collector/subscriber"
)

const (
	ServiceName    = "io.coderoso.robottle.svc.collector"
	ServiceVersion = "0.0.1"
)

func main() {
	var (
		configurationFile string

		errc = make(chan error)
	)
	service := micro.NewService(
		micro.Name(ServiceName),
		micro.Version(ServiceVersion),
		micro.Flags(
			cli.StringFlag{
				Name:        "config, c",
				Value:       "config.yml",
				Usage:       "Path to configuration file to use. Defaults to config.yml",
				EnvVar:      "CONFIG_FILE",
				Destination: &configurationFile,
			},
		),
		micro.RegisterTTL(30*time.Second),
		micro.RegisterInterval(10*time.Second),
	)
	service.Init()

	if err := commonConfig.SetConfigurationFile(configurationFile); err != nil {
		log.Fatalf("error settings configuration file %v", err)
	}

	config, err := commonConfig.GetConfig()
	if err != nil {
		log.Fatalf("error getting configuration: %v", err)
	}
	db, err := commonConnection.NewRethinkDBConnection("collector")
	if err != nil {
		log.Fatalf("error getting collector database: %v", err)
	}

	deviceDataRepository := repository.NewDeviceDataRepository(db)

	accountServiceSettings, found := config.Service["account"]
	if !found {
		log.Fatalf("no service configuration for account found")
	}

	messagingSettings, found := config.Messaging["collector"]
	if !found {
		log.Fatalf("no messaging configuration for collector found")
	}
	deviceDataConsumerInfo, found := config.Consumer["device_data"]
	if !found {
		log.Fatalf("consumer information for device_data not found")
	}
	consumerHandler, err := commonConsumer.NewConsumerHandler(messagingSettings)
	if err != nil {
		log.Fatalf("error getting consumer handler: %v", err)
	}
	go func() {
		if err := consumerHandler.Start(); err != nil {
			errc <- fmt.Errorf("error starting consumer handler: %v", err)
		}
	}()

	accountService := accountProto.NewAccountService(accountServiceSettings.URL(), client.DefaultClient)

	options := option.NewOptions(
		option.WithDeviceDataRepository(deviceDataRepository),
		option.WithAccountService(accountService),
		option.WithConsumerHandler(consumerHandler),
		option.WithDeviceDataConsumerInfo(deviceDataConsumerInfo),
	)

	accountSubscriber := subscriber.NewAccountSubscriber(options)
	if err := micro.RegisterSubscriber("account_created", service.Server(), accountSubscriber.Handle); err != nil {
		log.Fatalf("error registering account subscriber: %v", err)
	}

	deviceDataHandler := handler.NewDeviceDataStreamHandler(options)
	if err := proto.RegisterDeviceDataHandler(service.Server(), deviceDataHandler); err != nil {
		log.Fatalf("error egistering device data handler: %v", err)
	}

	go func() {
		if err := accountSubscriber.SubscribeConsumers(); err != nil {
			errc <- fmt.Errorf("error subscribing devices: %v", err)
		}
	}()

	go func() {
		if err := service.Run(); err != nil {
			errc <- err
		}
	}()

	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL)
		errc <- fmt.Errorf("%v", <-c)
	}()

	if err := <-errc; err != nil {
		log.Fatal(err)
	}
}
