package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/micro/cli"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-web"

	accountProto "gitlab.com/robottle/robottle_account/proto/account"
	commonConfig "gitlab.com/robottle/robottle_common/config"
	"gitlab.com/robottle/robottle_common/hashing"
	commonPublisher "gitlab.com/robottle/robottle_common/publisher"
	deviceProto "gitlab.com/robottle/robottle_device/proto/device"

	"gitlab.com/robottle/robottle_collector/option"
	"gitlab.com/robottle/robottle_collector/router"
)

const (
	ServiceName = "io.coderoso.robottle.api.collector"
	Version     = "0.0.1"
)

func main() {
	var (
		configurationFile string

		errc = make(chan error)
	)
	service := web.NewService(
		web.Name(ServiceName),
		web.Version(Version),
		web.Flags(
			cli.StringFlag{
				Name:        "config, c",
				Value:       "config.yml",
				Usage:       "Path to configuration file to use. Defaults to config.yml",
				EnvVar:      "CONFIG_FILE",
				Destination: &configurationFile,
			},
		),
		web.RegisterTTL(30*time.Second),
		web.RegisterInterval(10*time.Second),
	)
	_ = service.Init()

	if err := commonConfig.SetConfigurationFile(configurationFile); err != nil {
		log.Fatalf("error setting configuration file: %v", err)
	}

	config, err := commonConfig.GetConfig()
	if err != nil {
		log.Fatalf("error getting configuration: %v", err)
	}

	hashingStrategy, err := hashing.NewHashingStrategy(config.Hashing)
	if err != nil {
		log.Fatalf("error getting hashing strategy: %v", err)
	}

	deviceServiceSettings, found := config.Service["device"]
	if !found {
		log.Fatal("no service configuration found for device")
	}

	accountServiceSettings, found := config.Service["account"]
	if !found {
		log.Fatal("no service configuration found for account")
	}

	deviceDataConsumerInfo, found := config.Consumer["device_data"]
	if !found {
		log.Fatalf("consumer information for device_data not found")
	}
	messagingSettings, found := config.Messaging["collector"]
	if !found {
		log.Fatalf("messaging settings for collector not found")
	}
	deviceDataPublisher, err := commonPublisher.NewPublisher(messagingSettings)
	if err != nil {
		log.Fatalf("error getting publisher for device data: %v", err)
	}

	deviceService := deviceProto.NewDeviceService(deviceServiceSettings.URL(), client.DefaultClient)
	accountService := accountProto.NewAccountService(accountServiceSettings.URL(), client.DefaultClient)

	httpSettings := config.HTTP
	if httpSettings == nil {
		log.Fatal("HTTP settings not found")
	}

	options := option.NewOptions(
		option.WithAccountService(accountService),
		option.WithDeviceService(deviceService),
		option.WithDeviceDataPublisher(deviceDataPublisher),
		option.WithDeviceDataConsumerInfo(deviceDataConsumerInfo),
		option.WithHashingStrategy(hashingStrategy),
		option.WithAllowedMethods(httpSettings.AllowedMethods),
		option.WithAllowedHeaders(httpSettings.AllowedHeaders),
	)

	service.Handle("/", router.NewRouter(options))

	go func() {
		if err := service.Run(); err != nil {
			errc <- err
		}
	}()

	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL)
		errc <- fmt.Errorf("%v", <-c)
	}()

	if err := <-errc; err != nil {
		log.Fatal(err)
	}
}
