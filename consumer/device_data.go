package consumer

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"gitlab.com/robottle/robottle_collector/repository"

	"gitlab.com/robottle/robottle_collector/model"

	pb "github.com/golang/protobuf/proto"

	accountProto "gitlab.com/robottle/robottle_account/proto/account"
	commonConfig "gitlab.com/robottle/robottle_common/config"
	commonConsumer "gitlab.com/robottle/robottle_common/consumer"

	proto "gitlab.com/robottle/robottle_collector/proto/collector"
)

type DeviceDataConsumer struct {
	account              *accountProto.AccountResponse
	consumerInfo         *commonConfig.Consumer
	consumerErrorChannel chan error
	deviceDataRepository *repository.DeviceDataRepository
}

func NewDeviceDataConsumer(
	account *accountProto.AccountResponse,
	consumerInfo *commonConfig.Consumer,
	deviceDataRepository *repository.DeviceDataRepository,
) commonConsumer.Consumer {
	return &DeviceDataConsumer{
		account:              account,
		consumerInfo:         consumerInfo,
		consumerErrorChannel: make(chan error),
		deviceDataRepository: deviceDataRepository,
	}
}

func (c *DeviceDataConsumer) Consume(data []byte) error {
	message := &proto.DeviceDataMessage{}
	if err := pb.Unmarshal(data, message); err != nil {
		return fmt.Errorf("error unmarshaling message: %v", err)
	}
	timestamp, err := time.Parse(time.RFC3339, message.Timestamp)
	if err != nil {
		timestamp = time.Now()
	}
	ctx := context.Background()
	dataMessage := &model.Message{
		DeviceID:   message.DeviceId,
		AccountID:  message.AccountId,
		DeviceSlug: message.DeviceSlug,
		Sensor:     message.Sensor,
		Timestamp:  timestamp,
		Data:       (*json.RawMessage)(&message.Data),
	}
	if err := c.deviceDataRepository.InsertMessage(ctx, dataMessage); err != nil {
		log.Printf("%+v", dataMessage)
		return fmt.Errorf("error inserting data message: %v", err)
	}
	return nil
}

func (c *DeviceDataConsumer) GetExchange() string {
	return c.consumerInfo.Exchange
}

func (c *DeviceDataConsumer) GetQueue() string {
	return c.consumerInfo.Queue
}

func (c *DeviceDataConsumer) GetRoutingKey() string {
	return c.consumerInfo.RoutingKey
}

func (c *DeviceDataConsumer) GetTag() string {
	return c.consumerInfo.Tag
}

func (c *DeviceDataConsumer) GetErrorChannel() chan error {
	return c.consumerErrorChannel
}
