module gitlab.com/robottle/robottle_collector

go 1.12

require (
	github.com/gin-contrib/cors v0.0.0-20190424000812-bd1331c62cae
	github.com/gin-gonic/gin v1.3.0
	github.com/golang/protobuf v1.3.1
	github.com/micro/go-micro v1.1.0
	gitlab.com/robottle/robottle_account v0.0.0-20190503011625-7e0872a308a3
	gitlab.com/robottle/robottle_common v0.0.0-20190502075711-c703ea102562
	gitlab.com/robottle/robottle_device v0.0.0-00010101000000-000000000000
	go.mongodb.org/mongo-driver v1.0.1
	golang.org/x/sync v0.0.0-20190423024810-112230192c58 // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/rethinkdb/rethinkdb-go.v5 v5.0.1
)

replace gitlab.com/robottle/robottle_account => ../robottle_account

replace gitlab.com/robottle/robottle_common => ../robottle_common

replace gitlab.com/robottle/robottle_device => ../robottle_device
