package handler

import (
	"context"
	"log"
	"time"

	"gitlab.com/robottle/robottle_collector/model"

	"gitlab.com/robottle/robottle_collector/option"

	proto "gitlab.com/robottle/robottle_collector/proto/collector"
	"gitlab.com/robottle/robottle_collector/repository"
)

type DeviceDataStreamHandler struct {
	deviceDataRepository *repository.DeviceDataRepository
}

func NewDeviceDataStreamHandler(options *option.Options) proto.DeviceDataHandler {
	return &DeviceDataStreamHandler{
		deviceDataRepository: options.DeviceDataRepository,
	}
}

func (h *DeviceDataStreamHandler) ServerStream(ctx context.Context, req *proto.DeviceDataRequest, stream proto.DeviceData_ServerStreamStream) error {
	changes, err := h.deviceDataRepository.StreamDeviceData(ctx, req.AccountId, req.Sensor)
	if err != nil {
		log.Printf("error getting device data changes: %+v", err)
		return err
	}
	defer func() {
		_ = changes.Close()
	}()
	message := &model.Message{}
	for changes.Next(&message) {
		data, err := message.Data.MarshalJSON()
		if err != nil {
			return err
		}
		deviceDataMessage := &proto.DeviceDataMessage{
			AccountId:  message.AccountID,
			DeviceId:   message.DeviceID,
			DeviceSlug: message.DeviceSlug,
			Sensor:     message.Sensor,
			Timestamp:  message.Timestamp.Format(time.RFC3339),
			Data:       data,
		}
		if err := stream.Send(deviceDataMessage); err != nil {
			return err
		}
	}
	return nil
}

func (h *DeviceDataStreamHandler) Stream(ctx context.Context, stream proto.DeviceData_StreamStream) error {
	for {
		req, err := stream.Recv()
		if err != nil {
			return err
		}
		changes, err := h.deviceDataRepository.StreamDeviceData(ctx, req.AccountId, req.Sensor)
		if err != nil {
			return err
		}
		message := &model.Message{}
		for changes.Next(&message) {
			log.Printf("Stream - current: %+v", message)
		}
		_ = changes.Close()
	}
}
