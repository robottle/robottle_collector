package helper

import (
	"github.com/gin-gonic/gin"

	accountProto "gitlab.com/robottle/robottle_account/proto/account"
	deviceProto "gitlab.com/robottle/robottle_device/proto/device"
)

func GetDevice(ctx *gin.Context) *deviceProto.DeviceResponse {
	device, found := ctx.Get("device")
	if found {
		return device.(*deviceProto.DeviceResponse)
	}
	return nil
}

func GetAccount(ctx *gin.Context) *accountProto.AccountResponse {
	account, found := ctx.Get("account")
	if found {
		return account.(*accountProto.AccountResponse)
	}
	return nil
}
