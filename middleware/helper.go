package middleware

import (
	"strings"

	"github.com/gin-gonic/gin"
)

func findInRequest(headerKey string, paramKey string, ctx *gin.Context) (string, bool) {
	value := ctx.GetHeader(headerKey)
	if isEmpty(value) {
		value = ctx.Param(paramKey)
	}
	return value, !isEmpty(value)
}

func isEmpty(value string) bool {
	return len(strings.TrimSpace(value)) == 0
}
