package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"

	accountProto "gitlab.com/robottle/robottle_account/proto/account"
	deviceProto "gitlab.com/robottle/robottle_device/proto/device"

	"gitlab.com/robottle/robottle_collector/option"
)

const (
	deviceKeyHeaderKey = "X-Device-Key"
	deviceKeyParameter = "device_key"
)

func RequireDeviceKey(options *option.Options) gin.HandlerFunc {
	deviceService := options.DeviceService
	accountService := options.AccountService
	return func(ctx *gin.Context) {
		deviceKey, found := findInRequest(deviceKeyHeaderKey, deviceKeyParameter, ctx)
		if !found {
			ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": http.StatusBadRequest,
				"error":  "invalid device key",
			})
			return
		}
		deviceRequest := &deviceProto.FindDeviceRequest{
			DeviceKey: deviceKey,
		}
		deviceResponse, err := deviceService.FindDeviceByDeviceKey(ctx, deviceRequest)
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": http.StatusBadRequest,
				"error":  err,
			})
			return
		}
		accountRequest := &accountProto.FindAccountRequest{
			Id: deviceResponse.AccountId,
		}
		accountResponse, err := accountService.FindAccountByID(ctx, accountRequest)
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": http.StatusBadRequest,
				"error":  err,
			})
			return
		}
		ctx.Set("device", deviceResponse)
		ctx.Set("account", accountResponse)
		ctx.Next()
	}
}
