package middleware

import (
	"bytes"
	"io/ioutil"
	"net/http"

	"gitlab.com/robottle/robottle_collector/option"

	"gitlab.com/robottle/robottle_collector/helper"

	"github.com/gin-gonic/gin"
)

const (
	signatureHeaderKey    = "X-Signature"
	signatureParameterKey = "signature"
)

func ValidatePayload(options *option.Options) gin.HandlerFunc {
	hashingStrategy := options.HashingStrategy
	return func(ctx *gin.Context) {
		signature, found := findInRequest(signatureHeaderKey, signatureParameterKey, ctx)
		if !found {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"status": http.StatusUnauthorized,
				"error":  "invalid signature",
			})
			return
		}
		buff := make([]byte, 1024)
		num, _ := ctx.Request.Body.Read(buff)
		body := buff[0:num]
		ctx.Request.Body = ioutil.NopCloser(bytes.NewBuffer(body))

		device := helper.GetDevice(ctx)
		if err := hashingStrategy.ValidateHMAC(body, signature, device.DeviceSecret); err != nil {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"status": http.StatusUnauthorized,
				"error":  err.Error(),
			})
			return
		}
		ctx.Next()
	}
}
