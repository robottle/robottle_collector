package model

import (
	"encoding/json"
	"time"
)

type Message struct {
	ID         string           `json:"id" gorethink:"id,omitempty" rethinkdb:"id,omitempty"`
	DeviceID   int64            `json:"device_id" gorethink:"device_id" rethinkdb:"device_id"`
	AccountID  int64            `json:"account_id" gorethink:"account_id" rethinkdb:"account_id"`
	DeviceSlug string           `json:"slug" gorethink:"device_slug" rethinkdb:"device_slug"`
	Sensor     string           `json:"sensor,omitempty" gorethink:"sensor,omitempty" rethinkdb:"sensor"`
	Timestamp  time.Time        `json:"timestamp" gorethink:"timestamp" rethinkdb:"timestamp"`
	Data       *json.RawMessage `json:"data" gorethink:"data" rethinkdb:"data"`
}
