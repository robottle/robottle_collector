package option

import (
	accountProto "gitlab.com/robottle/robottle_account/proto/account"
	"gitlab.com/robottle/robottle_collector/repository"
	commonConfig "gitlab.com/robottle/robottle_common/config"
	commonConsumer "gitlab.com/robottle/robottle_common/consumer"
	"gitlab.com/robottle/robottle_common/hashing"
	commonPublisher "gitlab.com/robottle/robottle_common/publisher"
	deviceProto "gitlab.com/robottle/robottle_device/proto/device"
)

type Options struct {
	DeviceDataRepository   *repository.DeviceDataRepository
	DeviceService          deviceProto.DeviceService
	AccountService         accountProto.AccountService
	ConsumerHandler        commonConsumer.Handler
	DeviceDataPublisher    commonPublisher.Publisher
	DeviceDataConsumerInfo *commonConfig.Consumer
	HashingStrategy        hashing.Strategy
	AllowedMethods         []string
	AllowedHeaders         []string
}

type Option func(*Options)

func WithDeviceDataRepository(deviceDataRepository *repository.DeviceDataRepository) Option {
	return func(options *Options) {
		options.DeviceDataRepository = deviceDataRepository
	}
}

func WithAllowedMethods(allowedMethods []string) Option {
	return func(options *Options) {
		options.AllowedMethods = allowedMethods
	}
}

func WithAllowedHeaders(allowedHeaders []string) Option {
	return func(options *Options) {
		options.AllowedHeaders = allowedHeaders
	}
}

func WithHashingStrategy(hashingStrategy hashing.Strategy) Option {
	return func(options *Options) {
		options.HashingStrategy = hashingStrategy
	}
}

func WithDeviceService(deviceService deviceProto.DeviceService) Option {
	return func(options *Options) {
		options.DeviceService = deviceService
	}
}

func WithDeviceDataPublisher(publisher commonPublisher.Publisher) Option {
	return func(options *Options) {
		options.DeviceDataPublisher = publisher
	}
}

func WithDeviceDataConsumerInfo(consumerInfo *commonConfig.Consumer) Option {
	return func(options *Options) {
		options.DeviceDataConsumerInfo = consumerInfo
	}
}

func WithConsumerHandler(consumerHandler commonConsumer.Handler) Option {
	return func(options *Options) {
		options.ConsumerHandler = consumerHandler
	}
}

func WithAccountService(accountService accountProto.AccountService) Option {
	return func(options *Options) {
		options.AccountService = accountService
	}
}

func NewOptions(opts ...Option) *Options {
	options := &Options{}

	for _, opt := range opts {
		opt(options)
	}

	return options
}
