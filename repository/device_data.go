package repository

import (
	"context"
	"log"

	rethink "gopkg.in/rethinkdb/rethinkdb-go.v5"

	"gitlab.com/robottle/robottle_collector/model"
)

type DeviceDataRepository struct {
	session *rethink.Session
}

func NewDeviceDataRepository(session *rethink.Session) *DeviceDataRepository {
	return &DeviceDataRepository{
		session: session,
	}
}

func (r *DeviceDataRepository) InsertMessage(ctx context.Context, message *model.Message) (err error) {
	//var data map[string]interface{}
	//if err = json.Unmarshal(*message.Data, &data); err != nil {
	//	return
	//}
	//document := bson.M{
	//	"device_id":   message.DeviceID,
	//	"account_id":  message.AccountID,
	//	"device_slug": message.DeviceSlug,
	//	"sensor":      message.Sensor,
	//	"data":        data,
	//	"timestamp":   message.Timestamp,
	//}
	err = rethink.Table("device_data").Insert(message).Exec(r.session, rethink.ExecOpts{
		Context: ctx,
	})
	if err != nil {
		log.Printf("error: %+v", err)
	}
	return
}

func (r *DeviceDataRepository) StreamDeviceData(ctx context.Context, accountID int64, sensor string) (stream *rethink.Cursor, err error) {
	stream, err = rethink.
		Table("device_data").
		Filter(rethink.Row.Field("account_id").Eq(accountID)).
		Filter(rethink.Row.Field("sensor").Eq(sensor)).
		Changes().
		Field("new_val").
		Run(r.session)
	return
}
