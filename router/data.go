package router

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	pb "github.com/golang/protobuf/proto"
	"gitlab.com/robottle/robottle_collector/helper"

	commonConfig "gitlab.com/robottle/robottle_common/config"
	"gitlab.com/robottle/robottle_common/hashing"
	commonPublisher "gitlab.com/robottle/robottle_common/publisher"

	"gitlab.com/robottle/robottle_collector/option"
	proto "gitlab.com/robottle/robottle_collector/proto/collector"
)

type DataHandler struct {
	hashingStrategy        hashing.Strategy
	deviceDataPublisher    commonPublisher.Publisher
	deviceDataConsumerInfo *commonConfig.Consumer
}

func NewDataHandler(options *option.Options) *DataHandler {
	return &DataHandler{
		hashingStrategy:        options.HashingStrategy,
		deviceDataPublisher:    options.DeviceDataPublisher,
		deviceDataConsumerInfo: options.DeviceDataConsumerInfo,
	}
}

func (h *DataHandler) Create(ctx *gin.Context) {
	device := helper.GetDevice(ctx)
	account := helper.GetAccount(ctx)

	ctxCopy := ctx.Copy()
	go func() {
		data, err := ctxCopy.GetRawData()
		if err != nil {
			log.Printf("error getting device data: %v", err)
			return
		}
		sensor := ctx.Query("sensor")
		now := time.Now()
		message := &proto.DeviceDataMessage{
			DeviceId:   device.Id,
			AccountId:  account.Id,
			DeviceSlug: device.Slug,
			Sensor:     sensor,
			Data:       data,
			Timestamp:  now.Format(time.RFC3339),
		}
		consumerInfo := &commonConfig.Consumer{
			Exchange:   h.deviceDataConsumerInfo.Exchange,
			RoutingKey: fmt.Sprintf("%s-%s", h.deviceDataConsumerInfo.RoutingKey, account.Slug),
		}
		body, err := pb.Marshal(message)
		if err != nil {
			log.Printf("error marshaling device data message: %v", err)
			return
		}
		if err := h.deviceDataPublisher.Publish(consumerInfo, body); err != nil {
			log.Printf("error publishing message: %v", err)
		}
	}()

	ctx.Status(http.StatusNoContent)
}
