package router

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"gitlab.com/robottle/robottle_common/config"

	"gitlab.com/robottle/robottle_collector/middleware"
	"gitlab.com/robottle/robottle_collector/option"
)

func NewRouter(options *option.Options) *gin.Engine {
	router := gin.Default()
	if config.IsProduction() {
		gin.SetMode(gin.ReleaseMode)
	}
	router.RedirectTrailingSlash = true

	router.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
		AllowHeaders:    options.AllowedHeaders,
		AllowMethods:    options.AllowedMethods,
		ExposeHeaders: []string{
			"Content-Type",
			"Content-Length",
			"Access-Control-Allow-Headers",
			"Access-Control-Allow-Methods",
		},
	}))

	router.Use(middleware.RequireDeviceKey(options))
	router.Use(middleware.ValidatePayload(options))

	dataHandler := NewDataHandler(options)

	collector := router.Group("/collector")

	data := collector.Group("/data")
	{
		data.POST("/", dataHandler.Create)
	}

	return router
}
