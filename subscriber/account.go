package subscriber

import (
	"context"
	"fmt"

	"gitlab.com/robottle/robottle_collector/repository"

	"gitlab.com/robottle/robottle_collector/consumer"

	accountProto "gitlab.com/robottle/robottle_account/proto/account"
	commonConfig "gitlab.com/robottle/robottle_common/config"
	commonConsumer "gitlab.com/robottle/robottle_common/consumer"

	"gitlab.com/robottle/robottle_collector/option"
)

type AccountSubscriber struct {
	accountService         accountProto.AccountService
	deviceDataConsumerInfo *commonConfig.Consumer
	consumerHandler        commonConsumer.Handler
	deviceDataRepository   *repository.DeviceDataRepository
}

func NewAccountSubscriber(options *option.Options) *AccountSubscriber {
	return &AccountSubscriber{
		consumerHandler:        options.ConsumerHandler,
		deviceDataConsumerInfo: options.DeviceDataConsumerInfo,
		accountService:         options.AccountService,
		deviceDataRepository:   options.DeviceDataRepository,
	}
}

func (s *AccountSubscriber) SubscribeConsumers() error {
	ctx := context.Background()
	req := &accountProto.ListAccountsRequest{}
	res, err := s.accountService.ListAccounts(ctx, req)
	if err != nil {
		return err
	}
	for _, account := range res.Accounts {
		if err := s.subscribeAccount(ctx, account); err != nil {
			return err
		}
	}
	return nil
}

func (s *AccountSubscriber) Handle(ctx context.Context, account *accountProto.AccountResponse) (err error) {
	err = s.subscribeAccount(ctx, account)
	return
}

func (s *AccountSubscriber) subscribeAccount(ctx context.Context, account *accountProto.AccountResponse) error {
	consumerInfo := &commonConfig.Consumer{
		Exchange:   s.deviceDataConsumerInfo.Exchange,
		Queue:      fmt.Sprintf("%s-%s", s.deviceDataConsumerInfo.Queue, account.Slug),
		RoutingKey: fmt.Sprintf("%s-%s", s.deviceDataConsumerInfo.RoutingKey, account.Slug),
		Tag:        fmt.Sprintf("%s-%s", s.deviceDataConsumerInfo.Tag, account.Slug),
	}
	deviceDataConsumer := consumer.NewDeviceDataConsumer(account, consumerInfo, s.deviceDataRepository)
	if err := s.consumerHandler.AddConsumer(deviceDataConsumer); err != nil {
		return fmt.Errorf("error adding device data consumer for account %s: %v", account.Name, err)
	}
	return nil
}
